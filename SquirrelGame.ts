/// <reference path="Lib/phaser.d.ts"/>
/// <reference path="States/Preloader.ts"/>

module SquirrelGame {
    class SquirrelGame extends Phaser.Game {

        constructor(width?:number, height?:number) {
            super(width, height, Phaser.AUTO, 'phaser-div', {create: this.create});
        }

        create() {
            this.state.add("Preloader", Preloader, true)
        }
    }

    window.onload = () => {
        new SquirrelGame(1280, 720);
    }
}