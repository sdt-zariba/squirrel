/// <reference path="../Lib/phaser.d.ts"/>
/// <reference path="Game.ts"/>
/// <reference path="Boot.ts"/>
/// <reference path="MainMenu.ts"/>
///<reference path="GameOver.ts"/>
///<reference path="ShopState.ts"/>

module SquirrelGame {
    export class Preloader extends Phaser.State {
        preload() {
            this.game.load.atlasJSONArray('testDrawing2','Graphics/testDrawing2.png','Graphics/testDrawing2.json');
            this.game.load.atlasJSONArray('introSquirrel','Graphics/introSquirrel.png','Graphics/introSquirrel.json');
            this.game.load.atlasJSONArray('shopButtons','Graphics/buttons/shopButtons.png','Graphics/buttons/shopButtons.json');
            this.game.load.atlasJSONArray('squirrel', 'Graphics/squirrel_new/squirrel.png', 'Graphics/squirrel_new/squirrel.json');
            this.game.load.atlasJSONArray('trees', 'Graphics/trees.png', 'Graphics/trees.json');
            this.game.load.atlasJSONArray('wind','Graphics/Items/wind.png','Graphics/Items/wind.json');
            this.game.load.atlasJSONArray('flower','Graphics/Other/flower.png','Graphics/Other/flower.json');
            this.game.load.atlasJSONArray('arrowButton','Graphics/buttons/arrows.png','Graphics/buttons/arrows.json');
            this.game.load.atlasJSONArray('signs','Graphics/buttons/signs.png','Graphics/buttons/signs.json');
            this.game.load.atlasJSONArray('buttons',"Graphics/buttons/buttons.png",'Graphics/buttons/buttons.json');
            this.game.load.atlasJSONArray('bird',"Graphics/Items/owl.png","Graphics/Items/owl.json");
            this.game.load.image('shopBackground',"Graphics/Background/ShopBG.png");
            this.game.load.image('shopSign',"Graphics/Other/shopSign.png");
            this.game.load.image('star',"Graphics/Other/star.png");
            this.game.load.image('gameoverBackground',"Graphics/Background/GameoverBG.png");
            this.game.load.image('shopBackground',"Graphics/Background/ShopBG.png");
            this.game.load.image('background', 'Graphics/Background/background.png');
            this.game.load.image('background2', 'Graphics/Background/background2.png');
            this.game.load.image('grass', 'Graphics/Background/grass1.png');
            this.game.load.image('grass2', 'Graphics/Background/grass2.png');
            this.game.load.image('barBackgroundColour', 'Graphics/Bar/bar_background.png');
            this.game.load.image('barFrame', 'Graphics/Bar/bar_Frame.png');
            this.game.load.image('barForegroundColour', 'Graphics/Bar/bar_colour.png');
            this.game.load.image('acorn', 'Graphics/Collectibles/Acorn.png');
            this.game.load.image('spikes', 'Graphics/Items/Spikes.png');
            this.game.load.image('shieldIcon', 'Graphics/Items/ShieldIcon1.png');
            this.game.load.image('shield', 'Graphics/Items/Shield.png');
            this.game.load.image('GameOverBG','Graphics/Background/GameoverBG.png');
            this.game.load.image('mainMenuBG',"Graphics/Background/mainMenuBG.jpg");
            this.game.load.image('paperBG',"Graphics/paperBackground.jpg");
            this.game.load.image('pauseScreenBackground',"Graphics/Background/pauseScreenBG.png");
            this.game.load.audio('coinSound','Audio/Coin.wav');
            this.game.load.audio('buttonSound','Audio/Energy.wav');
            this.game.load.audio('gameOverSound','Audio/GameOver.wav');
            this.game.load.audio('jumpSound','Audio/Jump.wav');
            this.game.load.audio('soundtrack','Audio/SoundTrack.wav');
        }

        create() {
            this.initStates();
            this.setScale();
            this.game.state.start("Boot");
        }

        initStates() {
            this.game.state.add("Boot", Boot);
            this.game.state.add("MainMenu",MainMenu);
            this.game.state.add("Game", Game);
            this.game.state.add("GameOver",GameOver);
            this.game.state.add("ShopState",ShopState);
        }

        setScale() {
            this.game.scale.scaleMode = Phaser.ScaleManager.NO_SCALE;
        }
    }
}