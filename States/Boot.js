/// <reference path="../Lib/phaser.d.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var SquirrelGame;
(function (SquirrelGame) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            _super.apply(this, arguments);
        }
        Boot.prototype.preload = function () {
            this.game.load.image("zaribaLogo", "Graphics/zaribaLogo.png");
        };
        Boot.prototype.create = function () {
            var _this = this;
            this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
            //this.game.scale.scaleMode = Phaser.ScaleManager.NO_SCALE;
            //this.game.scale.scaleMode = Phaser.ScaleManager.RESIZE;
            //this.game.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;
            var bootLogo = this.game.add.image(this.game.width * 0.5, this.game.height * 0.5, "zaribaLogo");
            bootLogo.anchor.set(0.5, 0.5);
            this.game.time.events.add(2000, function () {
                _this.game.state.start("MainMenu");
            }, this);
        };
        return Boot;
    })(Phaser.State);
    SquirrelGame.Boot = Boot;
})(SquirrelGame || (SquirrelGame = {}));
//# sourceMappingURL=Boot.js.map