/// <reference path="../Lib/phaser.d.ts"/>
/// <reference path="../GameObjects/Tree.ts"/>
/// <reference path="../GameObjects/Player.ts"/>
///<reference path="../GameObjects/Background.ts"/>
///<reference path="..\GameObjects\Acorn.ts"/>
///<reference path="..\GameObjects\Flower.ts"/>
///<reference path="..\GameObjects\Wind.ts"/>
///<reference path="..\GameObjects\Bird.ts"/>
///<reference path="..\GameObjects\Shield.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var SquirrelGame;
(function (SquirrelGame) {
    function Rng(from, to) {
        return Math.round(Math.random() * (to - from) + from);
    }
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            _super.call(this);
            //milliseconds
            this.treeDistance = Math.round(Rng(3000, 4500));
            this.flowerDistance = Rng(100, 2500);
            this.flyingItems = [];
            this.powerUps = [];
            this.treeScale = 1;
            this.lastTreeScale = 1;
            this.flowerLocX = 50;
            this.treeLocX = 0;
        }
        Game.prototype.create = function () {
            // Create world physics
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.physics.arcade.gravity.y = 10;
            this.game.world.setBounds(0, 0, 1280, 720);
            this.debugKey = this.game.input.keyboard.addKey(Phaser.Keyboard.ONE);
            this.debugBoolean = false;
            this.soundTrack = this.game.add.audio('soundtrack', 2, true);
            this.buttonSound = this.game.add.audio('buttonSound', 1, false);
            this.gameOverSound = this.game.add.audio('gameOverSound', 1, false);
            this.soundTrack.play();
            this.isGamePaused = false;
            this.hasAcornBeenAdded = false;
            this.pauseTimer = 3000;
            this.pauseScreenBackground = this.game.add.image(0, 0, 'pauseScreenBackground');
            this.pauseScreenBackground.visible = false;
            this.background = new SquirrelGame.Background(this.game, 'background', 'background2', 1, 0);
            this.grass = new SquirrelGame.Background(this.game, 'grass', 'grass2', 2, this.game.height * 0.98);
            //initializing the groups
            this.frontGroup = this.add.group();
            this.flowerGroup = this.add.group();
            this.allTrees = [];
            // this.treesCollection = [];
            //spawning trees
            //this.treeTimer = new Phaser.Timer(this.game);
            //this.treeTimer.start();
            //this.treeTimer.pause();
            this.player = new SquirrelGame.Player(this.game);
            this.spawnTrees();
            this.SpawnFlowers();
            //var i=0;
            var playerPosX = this.allTrees[0].firstBranchX;
            var playerPosY = this.allTrees[0].firstBranchY;
            this.player.squirrel.position.x = playerPosX;
            this.player.squirrel.position.y = playerPosY;
            //this.deleteTrees();
            this.updateTreePos = this.game.width * 1.3;
            this.updateFlowerPos = this.game.width * 1.5;
            this.grass2 = new SquirrelGame.Background(this.game, 'grass2', 'grass', 3, this.game.height);
            this.frontGroup.addMultiple([this.flowerGroup, this.grass2.image, this.grass2.image2, this.grass2.image3, this.player.flightBar.backgroundFrame, this.player.flightBar.backgroundColour, this.player.flightBar.foregroundColour, this.player.hungerBar.backgroundFrame, this.player.hungerBar.backgroundColour, this.player.hungerBar.foregroundColour, this.player.squirrel, this.player.playerShield]);
            this.flyingItems = [];
            this.powerUps = [];
            this.spawnWindTimer = this.game.time.now + Phaser.Timer.SECOND * 5;
            this.spawnBirdTimer = this.game.time.now + Phaser.Timer.SECOND * 7;
            this.spawnShieldTimer = this.game.time.now + Phaser.Timer.SECOND * 10;
            var loadingStyle = { fill: "#A52A2A" };
            this.scoreText = this.game.add.text(50, 50, this.player.score.toString(), loadingStyle);
            this.scoreText.font = 'webfont';
            this.graphics = this.game.add.graphics(0, 0);
            this.graphics.beginFill(0x000000);
            this.graphics.drawCircle((this.game.width / 2) + 10, (this.game.height / 2) + 10, 50);
            this.graphics.endFill();
            this.graphics.visible = false;
            this.pauseTimerCDText = this.game.add.text(this.game.width / 2, this.game.height / 2, this.game.time.events.duration.toString() + " ", loadingStyle);
            this.pauseTimerCDText.visible = false;
            //arrow Buttons
            this.createButtons();
        };
        Game.prototype.exitGame = function () {
            this.buttonSound.play();
            this.game.destroy();
        };
        Game.prototype.goToMainMenu = function () {
            this.buttonSound.play();
            this.game.state.clearCurrentState();
            this.game.state.start('MainMenu');
        };
        Game.prototype.restartGame = function () {
            this.buttonSound.play();
            this.game.state.clearCurrentState();
            this.game.state.start('Game');
        };
        Game.prototype.resumeGame = function () {
            var _this = this;
            this.buttonSound.play();
            this.soundTrack.resume();
            this.graphics.visible = true;
            this.pauseButton.frame = 1;
            this.pauseTimerCDText.visible = true;
            this.resumeButton.inputEnabled = false;
            this.resumeButton.visible = false;
            this.resumeButtonText.visible = false;
            this.mainMenuButton.inputEnabled = false;
            this.mainMenuButton.visible = false;
            this.mainMenuButtonText.visible = false;
            this.exitButton.inputEnabled = false;
            this.exitButton.visible = false;
            this.exitButtonText.visible = false;
            this.restartButton.inputEnabled = false;
            this.restartButton.visible = false;
            this.restartButtonText.visible = false;
            this.pauseScreenBackground.visible = false;
            this.game.time.events.add(this.pauseTimer - 600, function () {
                _this.isGamePaused = false;
                _this.rightArrowButton.inputEnabled = true;
                _this.leftArrowButton.inputEnabled = true;
                _this.pauseButton.inputEnabled = true;
                _this.pauseTimerCDText.visible = false;
                _this.graphics.visible = false;
                _this.player.squirrel.body.allowGravity = true;
                _this.player.inputBG.inputEnabled = true;
                _this.player.squirrel.animations.currentAnim.paused = false;
                _this.player.squirrel.body.velocity.x = _this.playerCurrentVelocityX;
                _this.player.squirrel.body.velocity.y = _this.playerCurrentVelocityY;
                _this.flyingItems.forEach(function (item) {
                    item.sprite.body.velocity.x = item.speed;
                    item.sprite.animations.currentAnim.paused = false;
                });
                _this.pauseTimer = 3000;
            });
        };
        Game.prototype.pauseClicked = function () {
            this.buttonSound.play();
            this.isGamePaused = true;
            this.soundTrack.pause();
            this.resumeButton.frame = 1;
            this.playerCurrentVelocityX = this.player.squirrel.body.velocity.x;
            this.playerCurrentVelocityY = this.player.squirrel.body.velocity.y;
            this.player.squirrel.body.velocity.x = 0;
            this.player.squirrel.body.velocity.y = 0;
            this.player.squirrel.body.allowGravity = false;
            this.player.inputBG.inputEnabled = false;
            this.player.squirrel.animations.currentAnim.paused = true;
            this.flyingItems.forEach(function (item) {
                item.sprite.body.velocity.x = 0;
                item.sprite.animations.currentAnim.paused = true;
            });
            this.game.world.bringToTop(this.pauseScreenBackground);
            this.game.world.bringToTop(this.resumeButton);
            this.game.world.bringToTop(this.mainMenuButton);
            this.game.world.bringToTop(this.exitButton);
            this.game.world.bringToTop(this.restartButton);
            this.game.world.bringToTop(this.resumeButtonText);
            this.game.world.bringToTop(this.mainMenuButtonText);
            this.game.world.bringToTop(this.exitButtonText);
            this.game.world.bringToTop(this.restartButtonText);
            this.pauseScreenBackground.visible = true;
            this.resumeButton.inputEnabled = true;
            this.resumeButton.visible = true;
            this.resumeButtonText.visible = true;
            this.mainMenuButton.inputEnabled = true;
            this.mainMenuButton.visible = true;
            this.mainMenuButtonText.visible = true;
            this.exitButton.inputEnabled = true;
            this.exitButton.visible = true;
            this.exitButtonText.visible = true;
            this.restartButton.inputEnabled = true;
            this.restartButton.visible = true;
            this.restartButtonText.visible = true;
            this.pauseButton.inputEnabled = false;
            this.rightArrowButton.inputEnabled = false;
            this.leftArrowButton.inputEnabled = false;
        };
        Game.prototype.rightButtonUp = function () {
            this.player.rightButtonClicked = false;
        };
        Game.prototype.rightClicked = function () {
            this.player.rightButtonClicked = true;
        };
        Game.prototype.leftButtonUp = function () {
            this.player.leftButtonClicked = false;
        };
        Game.prototype.leftClicked = function () {
            this.player.leftButtonClicked = true;
        };
        Game.prototype.spawnWind = function () {
            if (this.spawnWindTimer < this.game.time.now) {
                this.flyingItems.push(new SquirrelGame.Wind(this.game, Rng(50, 600)));
                this.spawnWindTimer = this.game.time.now + Rng(3000, 6000);
            }
        };
        Game.prototype.spawnBird = function () {
            if (this.spawnBirdTimer < this.game.time.now) {
                this.flyingItems.push(new SquirrelGame.Bird(this.game, Rng(50, 600)));
                this.spawnBirdTimer = this.game.time.now + Rng(4000, 7000);
            }
        };
        Game.prototype.spawnShield = function () {
            if (this.spawnShieldTimer < this.game.time.now) {
                this.powerUps.push(new SquirrelGame.Shield(this.game, this.game.width * 1.1, Rng(50, 450)));
                this.spawnShieldTimer = this.game.time.now + Rng(30000, 40000);
            }
        };
        Game.prototype.updatePowerUps = function () {
            var _this = this;
            this.powerUps.forEach(function (item) {
                item.update(_this.player);
                item.sprite.x -= 2;
                if (item.sprite.width === -item.sprite.width || item.isTaken) {
                    item.sprite.destroy();
                    _this.powerUps.splice(_this.powerUps.lastIndexOf(item), 1);
                }
            });
        };
        Game.prototype.updateFlyingItems = function () {
            var _this = this;
            this.flyingItems.forEach(function (item) {
                item.update(_this.player);
                if (item.sprite.position.y === -item.sprite.width) {
                    item.sprite.destroy();
                    _this.flyingItems.splice(_this.flyingItems.lastIndexOf(item), 1);
                }
            });
        };
        Game.prototype.SpawnFlowers = function () {
            this.flowerLocX = 0;
            while (this.flowerLocX <= this.game.width * 1.4) {
                var flower = new SquirrelGame.Flower(this.game, this.flowerLocX);
                this.flowerGroup.add(flower);
                this.flowerLocX += Rng(200, 400);
            }
        };
        Game.prototype.SpawnNewFlowers = function () {
            if (this.game.time.now > this.flowerDistance) {
                this.flowerDistance = this.game.time.now + Rng(1500, 2500);
                var flower = new SquirrelGame.Flower(this.game, this.updateFlowerPos);
                this.updateFlowerPos += Rng(200, 400);
                this.flowerGroup.add(flower);
            }
        };
        Game.prototype.moveFlowers = function (movingSpeed) {
            this.flowerGroup.position.x -= movingSpeed;
        };
        Game.prototype.SpawnNewTree = function () {
            if (this.game.time.now > this.treeDistance) {
                this.treeScale = Rng(0.7, 1.1);
                this.treeDistance = this.game.time.now + Rng(2500, 4000);
                var group = this.game.add.group();
                var tree = new SquirrelGame.Tree(this.game, group, this.treeScale, this.updateTreePos);
                this.allTrees.push(tree);
            }
        };
        Game.prototype.spawnTrees = function () {
            this.treeLocX = 300;
            while (this.treeLocX - this.game.width <= 0) {
                this.treeScale = Rng(0.8, 1.3);
                var group = this.game.add.group();
                var tree = new SquirrelGame.Tree(this.game, group, this.treeScale, this.treeLocX);
                this.treeLocX += Math.round(this.lastTreeScale * Rng(400, 700));
                this.allTrees.push(tree);
                this.lastTreeScale = this.treeScale;
            }
        };
        Game.prototype.deleteTrees = function () {
            var _this = this;
            this.allTrees.forEach(function (tree) {
                if (tree.treeX <= -_this.game.width) {
                    tree.group.destroy();
                    _this.allTrees.splice(_this.allTrees.indexOf(tree), 1);
                }
            });
        };
        Game.prototype.moveTreesLeft = function () {
            var _this = this;
            var movingSpeed = 2;
            this.background.update();
            this.player.squirrel.x -= movingSpeed;
            this.updateTreePos += movingSpeed;
            this.allTrees.forEach(function (tree) {
                tree.acornCollection.forEach(function (acorn) {
                    acorn.sprite.x -= movingSpeed;
                }, _this);
                tree.spikeCollection.forEach(function (spike) {
                    spike.sprite.x -= movingSpeed;
                    spike.upsideDownSprite.x -= movingSpeed;
                }, _this);
                tree.group.x -= movingSpeed;
            }, this);
            this.grass.update();
            this.SpawnNewTree();
            this.moveFlowers(movingSpeed);
            this.SpawnNewFlowers();
            this.grass2.update();
        };
        Game.prototype.update = function () {
            var _this = this;
            if (!this.isGamePaused) {
                if (!this.player.isDead) {
                    this.scoreText.text = this.player.score.toString();
                    this.debugKey.onDown.add(this.toggle, this);
                    this.allTrees.forEach(function (tree) {
                        _this.game.physics.arcade.collide(_this.player.squirrel, tree.group);
                    }, this);
                    this.player.update();
                    this.allTrees.forEach(function (tree) {
                        tree.update(_this.player);
                    }, this);
                    this.moveTreesLeft();
                    this.deleteTrees();
                    this.spawnWind();
                    this.spawnBird();
                    this.spawnShield();
                    this.updateFlyingItems();
                    this.updatePowerUps();
                    this.game.world.bringToTop(this.frontGroup);
                }
                else {
                    this.player.squirrel.body.velocity.x = 0;
                    this.player.squirrel.body.velocity.y = 0;
                    this.gameOver();
                }
            }
            if (this.pauseTimerCDText.visible) {
                this.pauseTimerCDText.text = (Math.round(this.pauseTimer / 1000)).toString();
                this.pauseTimer -= this.game.time.elapsed;
            }
        };
        Game.prototype.gameOver = function () {
            var _this = this;
            this.soundTrack.destroy();
            this.leftArrowButton.inputEnabled = false;
            this.rightArrowButton.inputEnabled = false;
            this.player.inputBG.inputEnabled = false;
            this.pauseButton.inputEnabled = false;
            this.player.cursors.left.enabled = false;
            this.player.cursors.right.enabled = false;
            var maxScore = localStorage.getItem("maxScore");
            if (maxScore === null) {
                localStorage.setItem("maxScore", this.player.score.toString());
                maxScore = this.player.score;
            }
            else {
                if (parseInt(maxScore) <= this.player.score) {
                    maxScore = this.player.score;
                    localStorage.setItem("maxScore", this.player.score.toString());
                }
            }
            var acornCount = parseInt(localStorage.getItem("acornCount"));
            if (!this.hasAcornBeenAdded) {
                acornCount += this.player.acornCount;
                this.hasAcornBeenAdded = true;
            }
            localStorage.setItem("acornCount", acornCount.toString());
            this.game.time.events.add(500, function () {
                _this.gameOverSound.play();
                _this.game.state.clearCurrentState();
                _this.game.state.start("GameOver", false, false, maxScore, _this.player.score, _this.player.acornCount);
            });
        };
        Game.prototype.createButtons = function () {
            var loadingStyle = { fill: "#A52A2A" };
            this.leftArrowButton = this.game.add.button(100, 650, 'arrowButton', this.leftClicked, this, 2, 1, 0);
            this.leftArrowButton.anchor.set(0.5);
            this.leftArrowButton.scale.set(-1, 1);
            this.leftArrowButton.onInputUp.add(this.leftButtonUp, this);
            this.leftArrowButton.onInputDown.add(this.leftClicked, this);
            this.frontGroup.add(this.leftArrowButton);
            this.rightArrowButton = this.game.add.button(240, 650, 'arrowButton', this.rightClicked, this, 2, 1, 0);
            this.rightArrowButton.anchor.set(0.5);
            this.rightArrowButton.onInputUp.add(this.rightButtonUp, this);
            this.rightArrowButton.onInputDown.add(this.rightClicked, this);
            this.frontGroup.add(this.rightArrowButton);
            var buttonScaleX = 0.6;
            var buttonScaleY = 1.2;
            this.pauseButton = this.game.add.button(1000, 650, 'buttons', this.pauseClicked, this, 2, 1, 0);
            this.pauseButton.anchor.set(0.5);
            this.pauseButton.scale.set(buttonScaleX, buttonScaleY);
            this.frontGroup.add(this.pauseButton);
            this.pauseButtonText = this.game.add.text(1000, 650, "Pause ", loadingStyle);
            this.pauseButtonText.scale.set(1);
            this.pauseButtonText.anchor.set(0.5);
            this.frontGroup.add(this.pauseButtonText);
            this.resumeButton = this.game.add.button(this.game.width / 2, 200, 'buttons', this.resumeGame, this, 2, 1, 0);
            this.resumeButton.anchor.set(0.5);
            this.resumeButton.scale.set(buttonScaleX + .2, buttonScaleY + .2);
            this.resumeButton.inputEnabled = false;
            this.resumeButton.visible = false;
            this.resumeButtonText = this.game.add.text(this.game.width / 2, 200, "Resume ", loadingStyle);
            this.resumeButtonText.scale.set(1.2);
            this.resumeButtonText.anchor.set(0.5);
            this.resumeButtonText.visible = false;
            this.restartButton = this.game.add.button(this.game.width / 2, 300, 'buttons', this.restartGame, this, 2, 1, 0);
            this.restartButton.anchor.set(0.5);
            this.restartButton.scale.set(buttonScaleX, buttonScaleY);
            this.restartButton.inputEnabled = false;
            this.restartButton.visible = false;
            this.restartButtonText = this.game.add.text(this.game.width / 2, 300, "Restart ", loadingStyle);
            this.restartButtonText.font = 'webfont';
            this.restartButtonText.scale.set(1);
            this.restartButtonText.anchor.set(0.5);
            this.restartButtonText.visible = false;
            this.mainMenuButton = this.game.add.button(this.game.width / 2, 400, 'buttons', this.goToMainMenu, this, 2, 1, 0);
            this.mainMenuButton.anchor.set(0.5);
            this.mainMenuButton.scale.set(buttonScaleX, buttonScaleY);
            this.mainMenuButton.inputEnabled = false;
            this.mainMenuButton.visible = false;
            this.mainMenuButtonText = this.game.add.text(this.game.width / 2, 400, "Main Menu ", loadingStyle);
            this.mainMenuButtonText.scale.set(1);
            this.mainMenuButtonText.anchor.set(0.5);
            this.mainMenuButtonText.visible = false;
            this.exitButton = this.game.add.button(this.game.width / 2, 500, 'buttons', this.exitGame, this, 2, 1, 0);
            this.exitButton.anchor.set(0.5);
            this.exitButton.scale.set(buttonScaleX, buttonScaleY);
            this.exitButton.inputEnabled = false;
            this.exitButton.visible = false;
            this.exitButtonText = this.game.add.text(this.game.width / 2, 500, "Exit ", loadingStyle);
            this.exitButtonText.font = 'webfont';
            this.exitButtonText.scale.set(1);
            this.exitButtonText.anchor.set(0.5);
            this.exitButtonText.visible = false;
        };
        Game.prototype.render = function () {
            var _this = this;
            if (this.debugBoolean) {
                // Debug squirrel
                this.game.debug.body(this.player.squirrel);
                this.flyingItems.forEach(function (item) {
                    _this.game.debug.body(item.sprite);
                }, this);
                // call renderGroup on each of the alive members
                this.allTrees.forEach(function (tree) {
                    tree.group.forEachAlive(_this.renderGroup, _this);
                    tree.spikeCollection.forEach(function (spike) {
                        _this.game.debug.body(spike.sprite);
                    }, _this);
                }, this);
                //Debug glide time
                this.game.debug.text(this.player.glideTime.toString(), 100, 100, "#FFF");
                this.game.debug.text(this.player.hungerTime.toString(), 600, 100, "#FFF");
                this.game.debug.text(this.player.isShieldActive.toString(), 600, 500, "#FFF");
            }
        };
        Game.prototype.renderGroup = function (member) {
            this.game.debug.body(member);
        };
        Game.prototype.toggle = function () {
            this.debugBoolean = !this.debugBoolean;
            if (!this.debugBoolean) {
                this.game.debug.reset();
            }
        };
        return Game;
    })(Phaser.State);
    SquirrelGame.Game = Game;
})(SquirrelGame || (SquirrelGame = {}));
//# sourceMappingURL=Game.js.map