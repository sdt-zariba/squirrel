/// <reference path="../Lib/phaser.d.ts"/>
/// <reference path="../GameObjects/Tree.ts"/>
/// <reference path="../GameObjects/Player.ts"/>
///<reference path="../GameObjects/Background.ts"/>
///<reference path="..\GameObjects\Acorn.ts"/>
///<reference path="..\GameObjects\Flower.ts"/>
///<reference path="..\GameObjects\Wind.ts"/>
///<reference path="..\GameObjects\Bird.ts"/>
///<reference path="..\GameObjects\Shield.ts"/>


module SquirrelGame {
    function Rng(from, to) {
        return Math.round(Math.random() * (to - from) + from);
    }

    export class Game extends Phaser.State {
        player:Player;
        background:Background;
        grass:Background;
        grass2:Background;
        pauseScreenBackground:Phaser.Image;

        playerCurrentVelocityX:number;
        playerCurrentVelocityY:number;

        debugKey:Phaser.Key;
        debugBoolean:boolean;
        hasAcornBeenAdded:boolean;

        leftArrowButton:Phaser.Button;
        rightArrowButton:Phaser.Button;
        pauseButton:Phaser.Button;
        mainMenuButton:Phaser.Button;
        exitButton:Phaser.Button;
        restartButton:Phaser.Button;
        resumeButton:Phaser.Button;

        pauseButtonText:Phaser.Text;
        mainMenuButtonText:Phaser.Text;
        exitButtonText:Phaser.Text;
        resumeButtonText:Phaser.Text;
        restartButtonText:Phaser.Text;
        pauseTimerCDText:Phaser.Text;
        graphics:Phaser.Graphics;

        isGamePaused:boolean;
        pauseTimer:number;

        frontGroup:Phaser.Group;

        allTrees:Array <Tree>;

        //milliseconds
        treeDistance:number = Math.round(Rng(3000,4500) );
        flowerDistance:number = Rng(100,2500);

        flyingItems:Array<any> = [];
        powerUps:Array<any> = [];
        spawnWindTimer:number;
        spawnBirdTimer:number;
        spawnShieldTimer:number;

        flowerGroup:Phaser.Group;

        treeScale:number = 1;
        updateTreePos:number;
        updateFlowerPos:number;
        treeLocX:number;
        lastTreeScale:number = 1;

        flowerLocX:number = 50;

        scoreText:Phaser.Text;

        soundTrack:Phaser.Sound;
        buttonSound:Phaser.Sound;

        gameOverSound:Phaser.Sound;

        constructor() {
            super();
            this.treeLocX = 0;
        }


        create() {
            // Create world physics
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.physics.arcade.gravity.y = 10;
            this.game.world.setBounds(0, 0, 1280, 720);

            this.debugKey = this.game.input.keyboard.addKey(Phaser.Keyboard.ONE);
            this.debugBoolean = false;

            this.soundTrack= this.game.add.audio('soundtrack',2,true);
            this.buttonSound=this.game.add.audio('buttonSound',1,false);
            this.gameOverSound=this.game.add.audio('gameOverSound',1,false);
            this.soundTrack.play();

            this.isGamePaused = false;
            this.hasAcornBeenAdded = false;
            this.pauseTimer = 3000;

            this.pauseScreenBackground = this.game.add.image(0, 0, 'pauseScreenBackground');
            this.pauseScreenBackground.visible = false;

            this.background = new Background(this.game, 'background', 'background2', 1, 0);

            this.grass = new Background(this.game, 'grass', 'grass2', 2, this.game.height*0.98 );

            //initializing the groups
            this.frontGroup = this.add.group();
            this.flowerGroup = this.add.group();
            this.allTrees = [];
            // this.treesCollection = [];

            //spawning trees
            //this.treeTimer = new Phaser.Timer(this.game);
            //this.treeTimer.start();
            //this.treeTimer.pause();

            this.player = new Player(this.game);
            this.spawnTrees();
            this.SpawnFlowers();

            //var i=0;
            var playerPosX= this.allTrees[0].firstBranchX;
            var playerPosY =this.allTrees[0].firstBranchY;
            this.player.squirrel.position.x=playerPosX;
            this.player.squirrel.position.y=playerPosY;



            //this.deleteTrees();
            this.updateTreePos = this.game.width * 1.3;
            this.updateFlowerPos = this.game.width * 1.5;
            this.grass2 = new Background(this.game, 'grass2', 'grass',3, this.game.height);
            this.frontGroup.addMultiple([this.flowerGroup, this.grass2.image, this.grass2.image2, this.grass2.image3,
                this.player.flightBar.backgroundFrame, this.player.flightBar.backgroundColour, this.player.flightBar.foregroundColour,
                this.player.hungerBar.backgroundFrame, this.player.hungerBar.backgroundColour, this.player.hungerBar.foregroundColour,
                this.player.squirrel, this.player.playerShield]);

            this.flyingItems = [];
            this.powerUps = [];
            this.spawnWindTimer = this.game.time.now + Phaser.Timer.SECOND * 5;
            this.spawnBirdTimer = this.game.time.now + Phaser.Timer.SECOND * 7;
            this.spawnShieldTimer = this.game.time.now + Phaser.Timer.SECOND * 10;

            var loadingStyle = {fill: "#A52A2A"};
            this.scoreText = this.game.add.text(50, 50, this.player.score.toString(), loadingStyle);
            this.scoreText.font = 'webfont';

            this.graphics= this.game.add.graphics(0,0);
            this.graphics.beginFill(0x000000);
            this.graphics.drawCircle((this.game.width/2)+10,(this.game.height/2)+10,50);
            this.graphics.endFill();

            this.graphics.visible=false;
            this.pauseTimerCDText = this.game.add.text(this.game.width/2,this.game.height/2, this.game.time.events.duration.toString()+" ",loadingStyle);
            this.pauseTimerCDText.visible= false;


            //arrow Buttons
            this.createButtons();
        }

        exitGame() {
            this.buttonSound.play();
            this.game.destroy();
        }

        goToMainMenu() {
            this.buttonSound.play();
            this.game.state.clearCurrentState();
            this.game.state.start('MainMenu');
        }

        restartGame() {
            this.buttonSound.play();
            this.game.state.clearCurrentState();
            this.game.state.start('Game');
        }

        resumeGame() {
            this.buttonSound.play();
            this.soundTrack.resume();
            this.graphics.visible=true;
            this.pauseButton.frame =1;

            this.pauseTimerCDText.visible = true;
            this.resumeButton.inputEnabled = false;
            this.resumeButton.visible = false;
            this.resumeButtonText.visible = false;
            this.mainMenuButton.inputEnabled = false;
            this.mainMenuButton.visible = false;
            this.mainMenuButtonText.visible = false;
            this.exitButton.inputEnabled = false;
            this.exitButton.visible = false;
            this.exitButtonText.visible = false;
            this.restartButton.inputEnabled = false;
            this.restartButton.visible = false;
            this.restartButtonText.visible = false;
            this.pauseScreenBackground.visible = false;

            this.game.time.events.add(this.pauseTimer - 600 , ()=> {
                this.isGamePaused = false;
                this.rightArrowButton.inputEnabled = true;
                this.leftArrowButton.inputEnabled = true;
                this.pauseButton.inputEnabled = true;
                this.pauseTimerCDText.visible = false;
                this.graphics.visible=false;

                this.player.squirrel.body.allowGravity = true;
                this.player.inputBG.inputEnabled = true;
                this.player.squirrel.animations.currentAnim.paused = false;
                this.player.squirrel.body.velocity.x = this.playerCurrentVelocityX;
                this.player.squirrel.body.velocity.y = this.playerCurrentVelocityY;

                this.flyingItems.forEach((item)=> {
                    item.sprite.body.velocity.x = item.speed;
                    item.sprite.animations.currentAnim.paused = false;
                });
                this.pauseTimer = 3000;
            })
        }

        pauseClicked() {
            this.buttonSound.play();
            this.isGamePaused = true;

            this.soundTrack.pause();
            this.resumeButton.frame = 1;

            this.playerCurrentVelocityX = this.player.squirrel.body.velocity.x;
            this.playerCurrentVelocityY = this.player.squirrel.body.velocity.y;
            this.player.squirrel.body.velocity.x = 0;
            this.player.squirrel.body.velocity.y = 0;
            this.player.squirrel.body.allowGravity = false;
            this.player.inputBG.inputEnabled = false;
            this.player.squirrel.animations.currentAnim.paused = true;

            this.flyingItems.forEach((item)=> {
                item.sprite.body.velocity.x = 0;
                item.sprite.animations.currentAnim.paused = true;
            });

            this.game.world.bringToTop(this.pauseScreenBackground);
            this.game.world.bringToTop(this.resumeButton);
            this.game.world.bringToTop(this.mainMenuButton);
            this.game.world.bringToTop(this.exitButton);
            this.game.world.bringToTop(this.restartButton);
            this.game.world.bringToTop(this.resumeButtonText);
            this.game.world.bringToTop(this.mainMenuButtonText);
            this.game.world.bringToTop(this.exitButtonText);
            this.game.world.bringToTop(this.restartButtonText);

            this.pauseScreenBackground.visible = true;
            this.resumeButton.inputEnabled = true;
            this.resumeButton.visible = true;
            this.resumeButtonText.visible = true;
            this.mainMenuButton.inputEnabled = true;
            this.mainMenuButton.visible = true;
            this.mainMenuButtonText.visible = true;
            this.exitButton.inputEnabled = true;
            this.exitButton.visible = true;
            this.exitButtonText.visible = true;
            this.restartButton.inputEnabled = true;
            this.restartButton.visible = true;
            this.restartButtonText.visible = true;
            this.pauseButton.inputEnabled = false;
            this.rightArrowButton.inputEnabled = false;
            this.leftArrowButton.inputEnabled = false;
        }

        rightButtonUp() {
            this.player.rightButtonClicked = false;
        }

        rightClicked() {
            this.player.rightButtonClicked = true;
        }

        leftButtonUp() {
            this.player.leftButtonClicked = false;
        }

        leftClicked() {
            this.player.leftButtonClicked = true;
        }

        spawnWind() {
            if (this.spawnWindTimer < this.game.time.now) {
                this.flyingItems.push(new Wind(this.game, Rng(50, 600)));
                this.spawnWindTimer = this.game.time.now + Rng(3000, 6000);
            }
        }

        spawnBird() {
            if (this.spawnBirdTimer < this.game.time.now) {
                this.flyingItems.push(new Bird(this.game, Rng(50, 600)));
                this.spawnBirdTimer = this.game.time.now + Rng(4000, 7000);
            }
        }

        spawnShield(){
            if(this.spawnShieldTimer < this.game.time.now) {
                this.powerUps.push(new Shield(this.game,this.game.width*1.1,Rng(50, 450)));
                this.spawnShieldTimer = this.game.time.now + Rng(30000,40000);
            }
        }

        updatePowerUps(){
            this.powerUps.forEach((item)=>{
                item.update(this.player);
                item.sprite.x -= 2;
                if(item.sprite.width === -item.sprite.width || item.isTaken)
                {
                    item.sprite.destroy();
                    this.powerUps.splice(this.powerUps.lastIndexOf(item),1);
                }
            })
        }

        updateFlyingItems() {
            this.flyingItems.forEach((item)=> {
                item.update(this.player);
                if (item.sprite.position.y === -item.sprite.width) {
                    item.sprite.destroy();
                    this.flyingItems.splice(this.flyingItems.lastIndexOf(item), 1);
                }
            });
        }

        SpawnFlowers() {
            this.flowerLocX = 0;
            while (this.flowerLocX <= this.game.width * 1.4) {
                var flower = new Flower(this.game, this.flowerLocX);
                this.flowerGroup.add(flower);
                this.flowerLocX += Rng(200, 400);
            }
        }

        SpawnNewFlowers() {
            if (this.game.time.now > this.flowerDistance) {
                this.flowerDistance = this.game.time.now + Rng(1500, 2500);
                var flower = new Flower(this.game, this.updateFlowerPos);
                this.updateFlowerPos += Rng(200, 400);
                this.flowerGroup.add(flower);
            }
        }

        moveFlowers( movingSpeed:number) {
            this.flowerGroup.position.x -= movingSpeed;
        }

        SpawnNewTree() {
            if (this.game.time.now > this.treeDistance) {
                this.treeScale = Rng(0.7,1.1);
                this.treeDistance = this.game.time.now + Rng(2500,4000);
                var group = this.game.add.group();
                var tree = new Tree(this.game, group, this.treeScale, this.updateTreePos);
                this.allTrees.push(tree);
            }
        }

        spawnTrees() {
            this.treeLocX = 300;
            while (this.treeLocX - this.game.width <= 0) {
                this.treeScale = Rng(0.8,1.3);
                var group = this.game.add.group();
                var tree = new Tree(this.game, group, this.treeScale, this.treeLocX);
                this.treeLocX += Math.round(this.lastTreeScale * Rng(400,700) );
                this.allTrees.push(tree);
                this.lastTreeScale = this.treeScale;
            }
        }

        deleteTrees() {
            this.allTrees.forEach((tree)=> {
                if (tree.treeX <= -this.game.width) {

                    tree.group.destroy();
                    this.allTrees.splice(this.allTrees.indexOf(tree), 1);
                }
            });
        }

        moveTreesLeft() {

            var movingSpeed=2;
            this.background.update();
            this.player.squirrel.x -= movingSpeed;
            this.updateTreePos += movingSpeed;
            this.allTrees.forEach((tree)=> {

                tree.acornCollection.forEach((acorn)=> {
                    acorn.sprite.x -= movingSpeed;
                }, this);
                tree.spikeCollection.forEach((spike)=> {
                    spike.sprite.x -= movingSpeed;
                    spike.upsideDownSprite.x -= movingSpeed;
                },this);
                tree.group.x -= movingSpeed;
            }, this);
            this.grass.update();
            this.SpawnNewTree();
            this.moveFlowers(movingSpeed);
            this.SpawnNewFlowers();
            this.grass2.update();

        }


        update() {
            if (!this.isGamePaused) {
                if (!this.player.isDead) {
                    this.scoreText.text = this.player.score.toString();
                    this.debugKey.onDown.add(this.toggle, this);
                    this.allTrees.forEach((tree)=> {
                        this.game.physics.arcade.collide(this.player.squirrel, tree.group);
                    }, this);
                    this.player.update();
                    this.allTrees.forEach((tree)=> {
                        tree.update(this.player);
                    }, this);

                    this.moveTreesLeft();
                    this.deleteTrees();
                    this.spawnWind();
                    this.spawnBird();
                    this.spawnShield();
                    this.updateFlyingItems();
                    this.updatePowerUps();

                    this.game.world.bringToTop(this.frontGroup);
                }
                else {
                    this.player.squirrel.body.velocity.x = 0;
                    this.player.squirrel.body.velocity.y = 0;
                    this.gameOver();
                }
            }

            if(this.pauseTimerCDText.visible)
            {
                this.pauseTimerCDText.text = (Math.round(this.pauseTimer/1000)).toString();
                this.pauseTimer -= this.game.time.elapsed;
            }
        }

        gameOver() {
            this.soundTrack.destroy();
            this.leftArrowButton.inputEnabled = false;
            this.rightArrowButton.inputEnabled = false;
            this.player.inputBG.inputEnabled = false;
            this.pauseButton.inputEnabled = false;
            this.player.cursors.left.enabled = false;
            this.player.cursors.right.enabled = false;

            var maxScore = localStorage.getItem("maxScore");
            if (maxScore === null) {
                localStorage.setItem("maxScore", this.player.score.toString());
                maxScore = this.player.score;
            }
            else {
                if (parseInt(maxScore) <= this.player.score) {
                    maxScore = this.player.score;
                    localStorage.setItem("maxScore", this.player.score.toString());
                }
            }

            var acornCount = parseInt(localStorage.getItem("acornCount"));
            if (!this.hasAcornBeenAdded) {
                acornCount += this.player.acornCount;
                this.hasAcornBeenAdded = true;
            }
            localStorage.setItem("acornCount", acornCount.toString());

            this.game.time.events.add(500, ()=> {
                this.gameOverSound.play();
                this.game.state.clearCurrentState();
                this.game.state.start("GameOver", false, false, maxScore, this.player.score, this.player.acornCount);
            })
        }

        createButtons() {
            var loadingStyle = {fill: "#A52A2A"};
            this.leftArrowButton = this.game.add.button(100, 650, 'arrowButton', this.leftClicked, this, 2, 1, 0);
            this.leftArrowButton.anchor.set(0.5);
            this.leftArrowButton.scale.set(-1, 1);
            this.leftArrowButton.onInputUp.add(this.leftButtonUp, this);
            this.leftArrowButton.onInputDown.add(this.leftClicked, this);
            this.frontGroup.add(this.leftArrowButton);

            this.rightArrowButton = this.game.add.button(240, 650, 'arrowButton', this.rightClicked, this, 2, 1, 0);
            this.rightArrowButton.anchor.set(0.5);
            this.rightArrowButton.onInputUp.add(this.rightButtonUp, this);
            this.rightArrowButton.onInputDown.add(this.rightClicked, this);
            this.frontGroup.add(this.rightArrowButton);

            var buttonScaleX=0.6;
            var buttonScaleY=1.2;
            this.pauseButton = this.game.add.button(1000, 650, 'buttons', this.pauseClicked, this, 2, 1, 0);
            this.pauseButton.anchor.set(0.5);
            this.pauseButton.scale.set(buttonScaleX,buttonScaleY);
            this.frontGroup.add(this.pauseButton);
            this.pauseButtonText = this.game.add.text(1000, 650, "Pause ", loadingStyle);
            this.pauseButtonText.scale.set(1);
            this.pauseButtonText.anchor.set(0.5);
            this.frontGroup.add(this.pauseButtonText);

            this.resumeButton = this.game.add.button(this.game.width / 2, 200, 'buttons', this.resumeGame, this, 2, 1, 0);
            this.resumeButton.anchor.set(0.5);
            this.resumeButton.scale.set(buttonScaleX+.2,buttonScaleY+.2);
            this.resumeButton.inputEnabled = false;
            this.resumeButton.visible = false;
            this.resumeButtonText = this.game.add.text(this.game.width / 2, 200, "Resume ", loadingStyle);
            this.resumeButtonText.scale.set(1.2);
            this.resumeButtonText.anchor.set(0.5);
            this.resumeButtonText.visible = false;

            this.restartButton = this.game.add.button(this.game.width / 2, 300, 'buttons', this.restartGame, this, 2, 1, 0);
            this.restartButton.anchor.set(0.5);
            this.restartButton.scale.set(buttonScaleX,buttonScaleY);
            this.restartButton.inputEnabled = false;
            this.restartButton.visible = false;
            this.restartButtonText = this.game.add.text(this.game.width / 2, 300, "Restart ", loadingStyle);
            this.restartButtonText.font = 'webfont';
            this.restartButtonText.scale.set(1);
            this.restartButtonText.anchor.set(0.5);
            this.restartButtonText.visible = false;

            this.mainMenuButton = this.game.add.button(this.game.width / 2, 400, 'buttons', this.goToMainMenu, this, 2, 1, 0);
            this.mainMenuButton.anchor.set(0.5);
            this.mainMenuButton.scale.set(buttonScaleX,buttonScaleY);
            this.mainMenuButton.inputEnabled = false;
            this.mainMenuButton.visible = false;
            this.mainMenuButtonText = this.game.add.text(this.game.width / 2, 400, "Main Menu ", loadingStyle);
            this.mainMenuButtonText.scale.set(1);
            this.mainMenuButtonText.anchor.set(0.5);
            this.mainMenuButtonText.visible = false;

            this.exitButton = this.game.add.button(this.game.width / 2, 500, 'buttons', this.exitGame, this, 2, 1, 0);
            this.exitButton.anchor.set(0.5);
            this.exitButton.scale.set(buttonScaleX,buttonScaleY);
            this.exitButton.inputEnabled = false;
            this.exitButton.visible = false;
            this.exitButtonText = this.game.add.text(this.game.width / 2, 500, "Exit ", loadingStyle);
            this.exitButtonText.font = 'webfont';
            this.exitButtonText.scale.set(1);
            this.exitButtonText.anchor.set(0.5);
            this.exitButtonText.visible = false;

        }

        render() {
            if (this.debugBoolean) {
                // Debug squirrel
                this.game.debug.body(this.player.squirrel);
                this.flyingItems.forEach((item)=>{
                    this.game.debug.body(item.sprite);
                },this);
                // call renderGroup on each of the alive members
                this.allTrees.forEach((tree)=> {
                    tree.group.forEachAlive(this.renderGroup, this);
                    tree.spikeCollection.forEach((spike)=> {
                        this.game.debug.body(spike.sprite);
                    }, this);
                }, this);

                //Debug glide time
                this.game.debug.text(this.player.glideTime.toString(), 100, 100, "#FFF");
                this.game.debug.text(this.player.hungerTime.toString(), 600, 100, "#FFF");
                this.game.debug.text(this.player.isShieldActive.toString(), 600, 500, "#FFF");
                //this.game.debug.text(this.playerCurrentVelocityX.toString(), 500, 400, "#FFF");
                //this.game.debug.text(this.playerCurrentVelocityY.toString(), 600, 400, "#FFF");
                //this.game.debug.text("Time until event: " + this.game.time.events.duration, 500, 400);
                //this.game.debug.text(this.player.leftButtonClicked.toString(), 500, 400, "#FFF");
                //this.game.debug.text(this.player.rightButtonClicked.toString(), 600, 400, "#FFF");

                // Debug jump count
                //this.game.debug.text(this.player.jumpCounter.toString(), 100, 125, "#FFF")
                //this.game.debug.text("Touching: " + this.player.squirrel.body.touching.down.toString(),100,100,"#FFF");
                //this.game.debug.text("Blocked: "+ this.player.squirrel.body.blocked.down.toString(),100,150,"#FFF");
                //console.log(this.player.squirrel.body.touching.down || this.player.squirrel.body.blocked.down);
            }
        }

        renderGroup(member) {
            this.game.debug.body(member);
        }

        toggle() {
            this.debugBoolean = !this.debugBoolean;
            if (!this.debugBoolean) {
                this.game.debug.reset();
            }
        }
    }
}