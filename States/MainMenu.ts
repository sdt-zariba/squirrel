/// <reference path="../Lib/phaser.d.ts"/>

module SquirrelGame{
    export class MainMenu extends Phaser.State{

        mainMenuWallpaper:Phaser.Image;
        firstSign:Phaser.Image;
        secondSign:Phaser.Image;

        startButton: Phaser.Button;
        shopButton:Phaser.Button;
        exitButton:Phaser.Button;

        startButtonText:Phaser.Text;
        shopButtonText:Phaser.Text;
        exitButtonText:Phaser.Text;

        maxHungerTime:string;
        maxGlideTime:string;
        acornCount:string;

        buttonSound:Phaser.Sound;

        constructor(){
            super();
        }

        create(){
            //localStorage.setItem("acornCount", (2000000).toString());
            this.mainMenuWallpaper = this.game.add.sprite(0,0,"mainMenuBG");
            this.mainMenuWallpaper.scale.set(1);
            this.firstSign=this.game.add.image(this.game.width*0.3,this.game.height*0.95,'signs',1);
            this.firstSign.anchor.set(0.5,1);
            this.firstSign.scale.set(1.8);

            this.secondSign=this.game.add.image(this.game.width*0.7,this.game.height*0.90,'signs',0);
            this.secondSign.anchor.set(0.5,1);
            this.secondSign.scale.set(1.8);

            var loadingStyle = {fill: "#A52A2A"};

            this.maxHungerTime = localStorage.getItem("maxHungerTime");
            if (this.maxHungerTime === null) {
                this.maxHungerTime = (20000).toString();
                localStorage.setItem("maxHungerTime", (20000).toString());
            }

            this.maxGlideTime = localStorage.getItem("maxGlideTime");
            if (this.maxGlideTime === null) {
                this.maxGlideTime = (5000).toString();
                localStorage.setItem("maxGlideTime", (5000).toString());
            }


            this.acornCount = localStorage.getItem("acornCount");
            if (this.acornCount === null)
            {
                this.acornCount = (0).toString();
                localStorage.setItem("acornCount", (0).toString());
            }
                
            this.startButton = this.game.add.button(900,270,'buttons',this.startGame,this,0,1,2,0);
            this.startButton.alpha=0;
            this.startButton.anchor.set(0.5);
            this.startButton.scale.set(0.5,1.2);
            this.startButtonText=this.game.add.text(890,270,"Play ",loadingStyle);
            this.startButtonText.font='webfont';
            this.startButtonText.anchor.set(0.5,0.4);
            this.startButtonText.scale.set(1);

            this.shopButton = this.game.add.button(900,375,'buttons',this.openShop,this,0,1,2,0);
            this.shopButton.alpha=0;
            this.shopButton.anchor.set(0.5);
            this.shopButton.scale.set(0.5,1.2);
            this.shopButtonText=this.game.add.text(890,375,"Shop ",loadingStyle);
            this.shopButtonText.font='webfont';
            this.shopButtonText.anchor.set(0.5,0.4);
            this.shopButtonText.scale.set(1);


            this.exitButton = this.game.add.button(900,480,'buttons',this.exitGame,this,0,1,2,0);
            this.exitButton.alpha=0;
            this.exitButton.anchor.set(0.5);
            this.exitButton.scale.set(0.5,1.2);
            this.exitButtonText=this.game.add.text(890,480,"Exit",loadingStyle);
            this.exitButtonText.font='webfont';
            this.exitButtonText.anchor.set(0.5,0.4);
            this.exitButtonText.scale.set(1);
            this.buttonSound=this.game.add.audio('buttonSound',1,false);

        }

        openShop()
        {
            this.buttonSound.play();
            this.game.state.start('ShopState');
        }

        startGame(){
            this.buttonSound.play();
            this.game.state.start("Game");
        }

        exitGame(){
            this.buttonSound.play();
            this.game.destroy();
        }

    }
}