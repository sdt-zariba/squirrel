/// <reference path="../Lib/phaser.d.ts"/>

module  SquirrelGame{
    export class GameOver extends Phaser.State{
        score:number;
        maxScore:string;
        acornCount:number;
        playButton:Phaser.Button;
        mainMenuButton:Phaser.Button;
        exitButton:Phaser.Button;
        firstSign: Phaser.Image;
        secondSign: Phaser.Image;

        init(maxScore:string, score:number, acornCount:number)
        {
            this.score = score;
            this.maxScore = maxScore;
            this.acornCount = acornCount;
        }

        create()
        {
            //TODO add gameover image
            this.game.add.image(0,0,"GameOverBG");

            this.firstSign = this.game.add.image(this.game.width*0.7,this.game.height*0.9,'signs',0);
            this.firstSign.anchor.set(0.5,1);
            this.firstSign.scale.set(1.7);

            this.secondSign= this.game.add.image(this.game.width*0.3,this.game.height*0.9,'signs',2);
            this.secondSign.anchor.set(0.5,1);
            this.secondSign.scale.set(1.7);

            var loadingStyle = {fill: "#A52A2A"};

            this.playButton = this.game.add.button(900,290,'buttons',this.playGame,this,0,1,2,0);
            this.playButton.anchor.set(0.5);
            this.playButton.scale.set(0.5,1.2);
            this.playButton.alpha=0;
            var playButtonText=this.game.add.text(890,290,"Play Again ",loadingStyle);
            playButtonText.font='webfont';
            playButtonText.anchor.set(0.5,0.4);
            playButtonText.scale.set(1);

            this.mainMenuButton = this.game.add.button(900,385,'buttons',this.goToMainMenu,this,0,1,2,0);
            this.mainMenuButton.anchor.set(0.5);
            this.mainMenuButton.scale.set(0.5,1.2);
            this.mainMenuButton.alpha=0;
            var mainMenuButtonText=this.game.add.text(890,385,"Main Menu ",loadingStyle);
            mainMenuButtonText.font='webfont';
            mainMenuButtonText.anchor.set(0.5,0.4);
            mainMenuButtonText.scale.set(1);

            this.exitButton = this.game.add.button(900,485,'buttons',this.exitGame,this,0,1,2,0);
            this.exitButton.anchor.set(0.5);
            this.exitButton.scale.set(0.5,1.2);
            this.exitButton.alpha=0;
            var exitButtonText=this.game.add.text(890,485,"Exit ",loadingStyle);
            exitButtonText.font='webfont';
            exitButtonText.anchor.set(0.5,0.4);
            exitButtonText.scale.set(1);

            var scoreText = this.game.add.text(390,250,"Score: " ,loadingStyle);
            scoreText.font = 'webfont';
            scoreText.anchor.set(.5);
            var scoreSecondText = this.game.add.text(390,280,this.score.toString()+" ",loadingStyle);
            scoreSecondText.font = 'webfont';
            scoreSecondText.anchor.set(.5);

            var maxScoreText = this.game.add.text(390,390,"High Score: ",loadingStyle);
            maxScoreText.font = 'webfont';
            maxScoreText.anchor.set(.5);
            var maxScoreSecondText= this.game.add.text(390,420,this.maxScore.toString()+" ",loadingStyle);
            maxScoreSecondText.font='webfont';
            maxScoreSecondText.anchor.set(.5);

            var acornCountText = this.game.add.text(390,320,"Acorn Count: ",loadingStyle);
            acornCountText.font = 'webfont';
            acornCountText.anchor.set(.5);
            var acorCountSecondText= this.game.add.text(390,350,this.acornCount.toString()+" ",loadingStyle);
            acorCountSecondText.font='webfont';
            acorCountSecondText.anchor.set(.5);
        }

        exitGame()
        {
            this.game.destroy();
        }

        playGame()
        {
            this.game.state.clearCurrentState();
            this.game.state.start("Game");
        }

        goToMainMenu(){
            this.game.state.clearCurrentState();
            this.game.state.start("MainMenu");
        }
    }
}
