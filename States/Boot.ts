/// <reference path="../Lib/phaser.d.ts"/>

module SquirrelGame{
    export class Boot extends Phaser.State{
        preload(){
            this.game.load.image("zaribaLogo", "Graphics/zaribaLogo.png");
        }

        create(){
            this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
            //this.game.scale.scaleMode = Phaser.ScaleManager.NO_SCALE;
            //this.game.scale.scaleMode = Phaser.ScaleManager.RESIZE;
            //this.game.scale.scaleMode = Phaser.ScaleManager.USER_SCALE;
            var bootLogo:Phaser.Image = this.game.add.image
            (this.game.width*0.5,this.game.height*0.5, "zaribaLogo");
            bootLogo.anchor.set(0.5,0.5);

            this.game.time.events.add(2000, () => {
                this.game.state.start("MainMenu");
            },this);
        }
    }
}