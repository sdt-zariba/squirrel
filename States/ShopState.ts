/// <reference path="../Lib/phaser.d.ts"/>

module SquirrelGame{
    export class ShopState extends Phaser.State{
        MAX_HUNGER_UPGRADE = 40000;
        MAX_GLIDE_UPGRADE = 10000;

        maxHungerTime:string;
        maxGlideTime:string;
        acornCount: number;

        hungerUpgradeCounter:string;
        glideUpgradeCounter:string;

        hungerUpgradeCost:number;
        glideUpgradeCost:number;

        glideTimeButton:Phaser.Button;
        hungerTimeButton:Phaser.Button;

        acornCountText:Phaser.Text;
        hungerUpgradeCostText:Phaser.Text;
        glideUpgradeCostText:Phaser.Text;

        create() {
            //Handles Local Storage Variables
            this.maxHungerTime = localStorage.getItem("maxHungerTime");

            this.maxGlideTime = localStorage.getItem("maxGlideTime");

            this.acornCount = parseInt(localStorage.getItem("acornCount"));

            this.hungerUpgradeCounter = localStorage.getItem("hungerUpgradeCounter");
            if(this.hungerUpgradeCounter ===null)
            {
                this.hungerUpgradeCounter = (1).toString();
                localStorage.setItem("hungerUpgradeCounter", (1).toString());
            }

            this.glideUpgradeCounter = localStorage.getItem("glideUpgradeCounter");
            if(this.glideUpgradeCounter ===null)
            {
                this.glideUpgradeCounter = (1).toString();
                localStorage.setItem("glideUpgradeCounter", (1).toString());
            }

            this.glideUpgradeCost = 1000 * parseInt(this.glideUpgradeCounter);
            this.hungerUpgradeCost = 2000 * parseInt(this.hungerUpgradeCounter);

            //background
            this.game.add.image(0, 0, "shopBackground");

            //handles text and buttons
            var loadingStyle = {fill: "#A52A2A"};

            var closeButton = this.game.add.button(this.game.width / 2, 650, 'buttons', this.closeShop, this, 0, 1, 2, 0);
            closeButton.inputEnabled = true;
            closeButton.anchor.set(0.5);
            var closeButtonText = this.game.add.text(this.game.width / 2, 650, "Close", loadingStyle);
            closeButtonText.font = 'webfont';
            closeButtonText.anchor.set(0.5, 0.4);

            this.acornCountText = this.game.add.text(150,100,"Acorn Count: " + this.acornCount.toString(),loadingStyle);
            this.acornCountText.anchor.set(0.5);
            this.acornCountText.font = 'webfont';

            if(parseInt(this.maxHungerTime) >= this.MAX_HUNGER_UPGRADE) {
                this.hungerUpgradeCostText = this.game.add.text(940, 475, "Maxed Out", loadingStyle);
            }
            else {
                this.hungerUpgradeCostText = this.game.add.text(940, 475, "Cost: " + this.hungerUpgradeCost.toString(), loadingStyle);
            }
            this.hungerUpgradeCostText.anchor.set(0.5);
            this.hungerUpgradeCostText.font = 'webfont';

            if(parseInt(this.maxGlideTime) >= this.MAX_GLIDE_UPGRADE) {
                this.glideUpgradeCostText = this.game.add.text(335, 475, "Maxed Out", loadingStyle);
            }
            else {
                this.glideUpgradeCostText = this.game.add.text(335, 475, "Cost: " + this.glideUpgradeCost.toString(), loadingStyle);
            }
            this.glideUpgradeCostText.anchor.set(0.5);
            this.glideUpgradeCostText.font = 'webfont';

            var glideInfoText = this.game.add.text(335,200,"Upgrade Maximum Glide Time",loadingStyle);
            glideInfoText.font = 'webfont';
            glideInfoText.anchor.set(0.5);

            var hungerInfoText = this.game.add.text(940,200,"Upgrade Maximum Hunger Time",loadingStyle);
            hungerInfoText.font = 'webfont';
            hungerInfoText.anchor.set(0.5);

            if(parseInt(this.maxGlideTime) >= this.MAX_GLIDE_UPGRADE || this.acornCount < this.glideUpgradeCost)
            {
                this.glideTimeButton = this.game.add.button(335, 360, 'shopButtons', this.addGlideTime, this);
                this.glideTimeButton.inputEnabled = false;
            }
            else
            {
                this.glideTimeButton = this.game.add.button(335, 360, 'shopButtons', this.addGlideTime, this,0,1,2,0);
            }
            this.glideTimeButton.anchor.set(0.5);
            this.glideTimeButton.scale.set(0.7);

            if (parseInt(this.maxHungerTime) >= this.MAX_HUNGER_UPGRADE || this.acornCount < this.hungerUpgradeCost) {
                this.hungerTimeButton = this.game.add.button(940, 360, 'shopButtons', this.addHungerTime, this);
                this.hungerTimeButton.inputEnabled = false;
            }
            else{

                this.hungerTimeButton = this.game.add.button(940, 360, 'shopButtons', this.addHungerTime, this, 4,3,5,4);
                this.hungerTimeButton.frame = 4;
        }
            this.hungerTimeButton.anchor.set(0.5);
            this.hungerTimeButton.scale.set(0.7);
        }

        closeShop()
        {
            this.game.state.start('MainMenu');
        }

        addHungerTime()
        {
            var hungerUpgradeNumber = parseInt(this.hungerUpgradeCounter);
            var maxHungerNumber = parseInt(this.maxHungerTime);
            if(maxHungerNumber < this.MAX_HUNGER_UPGRADE && this.acornCount >= this.hungerUpgradeCost)
            {
                maxHungerNumber += 5000;
                this.maxHungerTime = maxHungerNumber.toString();
                localStorage.setItem("maxHungerTime", maxHungerNumber.toString());
                hungerUpgradeNumber++;
                this.hungerUpgradeCounter = hungerUpgradeNumber.toString();
                localStorage.setItem("hungerUpgradeCounter",this.hungerUpgradeCounter);
                this.acornCount -= this.hungerUpgradeCost;
                localStorage.setItem("acornCount",this.acornCount.toString());
            }
        }

        addGlideTime()
        {
            var glideUpgradeNumber = parseInt(this.glideUpgradeCounter);
            var maxGlideNumber = parseInt(this.maxGlideTime);
            if(maxGlideNumber < this.MAX_GLIDE_UPGRADE && this.acornCount >= this.glideUpgradeCost)
            {
                maxGlideNumber += 1000;
                this.maxGlideTime = maxGlideNumber.toString();
                localStorage.setItem("maxGlideTime", maxGlideNumber.toString());
                glideUpgradeNumber++;
                this.glideUpgradeCounter = glideUpgradeNumber.toString();
                localStorage.setItem("glideUpgradeCounter",this.glideUpgradeCounter);
                this.acornCount -= this.glideUpgradeCost;
                localStorage.setItem("acornCount", this.acornCount.toString());
            }
        }

        update()
        {
            this.acornCountText.text = "Acorn Count: " + this.acornCount.toString();

            if(parseInt(this.maxGlideTime) >= this.MAX_GLIDE_UPGRADE){
                this.glideUpgradeCostText.text = "Maxed Out";
            }
            else {
                this.glideUpgradeCostText.text = "Cost: " + this.glideUpgradeCost.toString();
            }

            if(parseInt(this.maxHungerTime) >= this.MAX_HUNGER_UPGRADE){
                this.hungerUpgradeCostText.text = "Maxed Out";
            }
            else {
                this.hungerUpgradeCostText.text = "Cost: " + this.hungerUpgradeCost.toString();
            }

            this.glideUpgradeCost = 1000 * parseInt(this.glideUpgradeCounter);
            this.hungerUpgradeCost = 2000 * parseInt(this.hungerUpgradeCounter);

            if (parseInt(this.maxHungerTime) >= this.MAX_HUNGER_UPGRADE || this.acornCount < this.hungerUpgradeCost) {
                this.hungerTimeButton.frame = 3;
                this.hungerTimeButton.inputEnabled = false;
            }
            else
            {
                this.hungerTimeButton.inputEnabled = true;
            }

            if(parseInt(this.maxGlideTime) >= this.MAX_GLIDE_UPGRADE || this.acornCount < this.glideUpgradeCost)
            {
                this.glideTimeButton.frame = 1;
                this.glideTimeButton.inputEnabled = false;
            }
            else
            {
                this.glideTimeButton.inputEnabled = true;
            }
        }
    }
}