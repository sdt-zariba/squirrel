/// <reference path="Lib/phaser.d.ts"/>
/// <reference path="States/Preloader.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var SquirrelGame;
(function (_SquirrelGame) {
    var SquirrelGame = (function (_super) {
        __extends(SquirrelGame, _super);
        function SquirrelGame(width, height) {
            _super.call(this, width, height, Phaser.AUTO, 'phaser-div', { create: this.create });
        }
        SquirrelGame.prototype.create = function () {
            this.state.add("Preloader", _SquirrelGame.Preloader, true);
        };
        return SquirrelGame;
    })(Phaser.Game);
    window.onload = function () {
        new SquirrelGame(1280, 720);
    };
})(SquirrelGame || (SquirrelGame = {}));
//# sourceMappingURL=SquirrelGame.js.map