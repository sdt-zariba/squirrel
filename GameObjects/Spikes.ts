/// <reference path="../Lib/phaser.d.ts"/>
///<reference path="Player.ts"/>

module SquirrelGame{
    export class Spikes {
        game:Phaser.Game;
        sprite:Phaser.Sprite;
        upsideDownSprite:Phaser.Sprite;

        constructor(game:Phaser.Game, x:number, y:number) {
            this.game = game;
            this.sprite = this.game.add.sprite(x,y,'spikes');
            this.game.physics.arcade.enable(this.sprite);
            this.sprite.body.allowGravity = false;
            this.sprite.body.immovable = true;
            this.sprite.body.checkCollision.top = false;
            this.sprite.anchor.set(0.5);
            this.sprite.scale.setTo(0.3);
            this.sprite.body.offset.y = 10;

            this.upsideDownSprite = this.game.add.sprite(x,y + this.sprite.height,'spikes');
            this.game.physics.arcade.enable(this.upsideDownSprite);
            this.upsideDownSprite.body.allowGravity = false;
            this.upsideDownSprite.body.immovable = true;
            this.upsideDownSprite.body.checkCollision.top = false;
            this.upsideDownSprite.anchor.set(0.5);
            this.upsideDownSprite.scale.setTo(-0.3);
            this.upsideDownSprite.body.offset.y = 10;
        }

        update(player:Player) {
            //console.log(this.sprite.body.intersects(player.squirrel));

            if (this.game.physics.arcade.intersects(player.squirrel.body,this.sprite.body) && !player.isDead && !player.isShieldActive)
            {
                player.isDead = true;
            }
            if (this.game.physics.arcade.intersects(player.squirrel.body,this.upsideDownSprite.body) && !player.isDead && !player.isShieldActive)
            {
                player.isDead = true;
            }
        }
    }
}