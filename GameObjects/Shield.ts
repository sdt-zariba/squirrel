/// <reference path="../Lib/phaser.d.ts"/>
///<reference path="Player.ts"/>
module SquirrelGame {
    export class Shield {
        game:Phaser.Game;
        sprite:Phaser.Sprite;
        isTaken:boolean=false;

        constructor(game:Phaser.Game, x:number, y:number) {
            this.game = game;
            this.sprite = this.game.add.sprite(x, y, 'shieldIcon');
            this.game.physics.arcade.enable(this.sprite);
            this.sprite.body.allowGravity = false;
            this.sprite.body.immovable = true;
            this.sprite.body.checkCollision.top = false;
            this.sprite.anchor.set(0.5);
            this.sprite.scale.setTo(0.3);

            this.isTaken = false;
        }

        update(player:Player) {
            if (this.game.physics.arcade.intersects(player.squirrel.body, this.sprite.body) && !this.isTaken) {
                    player.isShieldTaken = true;
                    this.sprite.alpha = 0;
                    this.isTaken = true;
                }

            }
    }
}

