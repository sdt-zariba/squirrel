/// <reference path="../Lib/phaser.d.ts"/>

module SquirrelGame{
    function Rng(from, to) {
        return Math.round(Math.random() * (to - from) + from);
    }

    export class Flower extends Phaser.Sprite{
        game:Phaser.Game;
        posY:number;

        constructor(game:Phaser.Game,posX:number){
            this.game=game;
            this.posY=this.game.height-20;
            var possibleFrames=Rng(2,10);
            super(this.game,posX,this.posY,'flower',possibleFrames);
            this.anchor.set(0.5,1);
            this.scale.set(0.5);
        }
    }
}

