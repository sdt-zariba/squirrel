/// <reference path="../Lib/phaser.d.ts"/>
var SquirrelGame;
(function (SquirrelGame) {
    var Bar = (function () {
        function Bar(game, x, y) {
            this.game = game;
            this.backgroundFrame = this.game.add.sprite(x, y, 'barFrame');
            this.backgroundColour = this.game.add.sprite(x, y, 'barBackgroundColour');
            this.foregroundColour = this.game.add.sprite(x, y + 3, 'barForegroundColour');
        }
        Bar.prototype.create = function () {
        };
        return Bar;
    })();
    SquirrelGame.Bar = Bar;
})(SquirrelGame || (SquirrelGame = {}));
//# sourceMappingURL=Bar.js.map