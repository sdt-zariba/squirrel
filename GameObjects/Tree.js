/// <reference path="../Lib/phaser.d.ts"/>
/// <reference path="../States/Game.ts"/>
///<reference path="..\GameObjects\Acorn.ts"/>
///<reference path="Spikes.ts"/>
var SquirrelGame;
(function (SquirrelGame) {
    function treesRng(from, to) {
        return Math.round(Math.random() * (to - from) + from);
    }
    var Tree = (function () {
        function Tree(game, group, scale, locationX) {
            this.spawnBranch = false;
            this.leftBranch = false;
            this.rightBranch = false;
            this.firstBranch = true;
            this.acornCollection = [];
            this.spikeCollection = [];
            this.game = game;
            this.group = group;
            this.scale = scale;
            this.treeX = locationX;
            this.createTrunk();
        }
        Tree.prototype.create = function () {
            this.treeY = 400;
            var image1 = this.game.add.sprite(this.treeX - 2 * this.scale, this.treeY + 220 * this.scale, 'trees', 0);
            var image2 = this.game.add.sprite(this.treeX, this.treeY + 77 * this.scale, 'trees', 2);
            var image3 = this.game.add.sprite(this.treeX, this.treeY - 18 * this.scale, 'trees', 3);
            var image4 = this.game.add.sprite(this.treeX + 13 * this.scale, this.treeY + 75 * this.scale, 'trees', 5);
            var image5 = this.game.add.sprite(this.treeX, this.treeY - 113 * this.scale, 'trees', 2);
            var image6 = this.game.add.sprite(this.treeX - 100 * this.scale, this.treeY - 75 * this.scale, 'trees', 4);
            image1.scale.set(this.scale);
            image2.scale.set(this.scale);
            image3.scale.set(this.scale);
            image4.scale.set(this.scale);
            image5.scale.set(this.scale);
            image6.scale.set(this.scale);
            image1.anchor.set(0.5);
            image2.anchor.set(0.5);
            image3.anchor.set(0.5);
            image4.anchor.set(0);
            image5.anchor.set(0.5);
            image6.anchor.set(0.5);
            this.game.physics.arcade.enable([image4, image6]);
            image4.body.allowGravity = false;
            image4.body.immovable = true;
            image4.body.height = 20;
            image4.body.offset.y = 30 * this.scale;
            image6.body.allowGravity = false;
            image6.body.immovable = true;
            image6.body.height = 20;
            image6.body.offset.y = 10 * this.scale;
            this.group.add(image1);
            this.group.add(image2);
            this.group.add(image3);
            this.group.add(image4);
            this.group.add(image5);
            this.group.add(image6);
        };
        //creating tree base
        Tree.prototype.createTrunk = function () {
            //Y poziciqta, na koqto se generira durvoto
            this.treeY = this.game.height - 10;
            //sprites stuff
            var trunk = this.game.add.sprite(this.treeX, this.treeY * this.scale, 'trees', 0);
            trunk.anchor.set(0.5, 1);
            trunk.scale.set(this.scale);
            //current and next positions- for adding more branch pieces
            this.currentPosition = trunk.position.y = this.treeY * this.scale;
            this.nextPosition = this.treeY - trunk.height + 1;
            //adding the sprite to the trees group
            this.group.add(trunk);
            //generating next trunk and branch
            this.addBranchPiece();
        };
        Tree.prototype.addBranchPiece = function () {
            while (true) {
                this.addTrunkPiece();
                this.addBranch();
                if (this.nextPosition <= 0) {
                    break;
                }
            }
        };
        Tree.prototype.addNextBranchPiece = function (group) {
            var numOfTrees = group.length;
            var lastPiece = group.getChildAt(numOfTrees - 1);
            var lastPiecePos = lastPiece.position.y;
            if (lastPiecePos < 0) {
                //this.addTrunkPiece(lastPiecePos);
                this.addBranch();
            }
        };
        Tree.prototype.addTrunkPiece = function () {
            // console.log(this.lastTrunkFrame);
            //choosing the images for trunks
            var trunkFrame = treesRng(2, 3);
            //making sure no leafy trunk spawns in the bad place
            if ((this.lastTrunkFrame === 3 && trunkFrame === 3) || this.lastTrunkFrame === undefined) {
                trunkFrame = 2;
            }
            //allow adding a branch only to blank trunks
            if (trunkFrame === 2) {
                this.spawnBranch = true;
            }
            //drawing
            this.currentPosition = this.nextPosition;
            var trunkPiece = this.game.add.sprite(this.treeX + 3, this.currentPosition, 'trees', trunkFrame);
            trunkPiece.anchor.set(0.5, 1);
            trunkPiece.scale.set(this.scale);
            //trunkPiece.position.y = this.currentPosition;
            //finding the midpoint, to place the branch
            this.halfTrunkPos = this.currentPosition - ((trunkPiece.height) / 2);
            //finding the point to place the next trunk piece
            this.nextPosition -= trunkPiece.height - 1;
            //adding the sprite to the group
            this.group.add(trunkPiece);
            //saving it for the next trunk, so no leafy trunk spawns next to each other
            this.lastTrunkFrame = trunkFrame;
            // console.log(this.nextPosition);
            //return  this.nextPosition;
        };
        Tree.prototype.addBranch = function () {
            var spawnNumber = treesRng(0, 100);
            // left branch
            if (this.leftBranch === false && this.spawnBranch === true) {
                var branchPosX = (this.treeX) - 13 * this.scale;
                var branchPosY = this.halfTrunkPos - 13 * this.scale;
                //drawing and sprite stuff
                if (branchPosY > 50) {
                    var branch = this.game.add.sprite(branchPosX, branchPosY, 'trees', 4);
                    var x = (this.treeX - branch.width / 2) - 30 * this.scale;
                    var y = (this.halfTrunkPos - 10) - 13 * this.scale;
                    if (this.firstBranch) {
                        this.firstBranchX = x;
                        this.firstBranchY = y;
                    }
                    branch.anchor.set(1, 0.5);
                    branch.scale.set(this.scale);
                    if (spawnNumber > 25 && spawnNumber <= 70) {
                        this.spawnAcorns(x, y, treesRng(1, 3));
                    }
                    else if (spawnNumber > 75 && !this.firstBranch) {
                        this.spawnSpikes(x, y, treesRng(1, 4));
                    }
                    else if (this.firstBranch) {
                        this.firstBranch = false;
                    }
                    //physics..
                    this.game.physics.arcade.enable(branch);
                    branch.body.allowGravity = false;
                    branch.body.immovable = true;
                    branch.body.height = 20;
                    branch.body.offset.y = 10 * this.scale;
                    //adding sprite to group
                    this.group.add(branch);
                    //booleans for repeating left and right branches
                    this.spawnBranch = false;
                    this.leftBranch = true;
                    this.rightBranch = false;
                }
            }
            else if (this.rightBranch === false && this.spawnBranch === true) {
                //drawing
                var branchPosX = (this.treeX) + 13 * this.scale;
                var branchPosY = this.halfTrunkPos - 13 * this.scale;
                if (branchPosY > 50) {
                    var branch = this.game.add.sprite(branchPosX, branchPosY, 'trees', 5);
                    branch.anchor.set(0, 0.5);
                    branch.scale.set(this.scale);
                    var x = (this.treeX + branch.width / 2) + 13 * this.scale;
                    var y = (this.halfTrunkPos - 10) - 13 * this.scale;
                    if (spawnNumber > 25 && spawnNumber <= 70) {
                        this.spawnAcorns(x, y, treesRng(1, 3));
                    }
                    else if (spawnNumber > 75) {
                        this.spawnSpikes(x, y, treesRng(1, 4));
                    }
                    //physics
                    this.game.physics.arcade.enable(branch);
                    branch.body.allowGravity = false;
                    branch.body.immovable = true;
                    branch.body.height = 20;
                    branch.body.offset.y = 10 * this.scale;
                    //the same as above
                    this.group.add(branch);
                    this.spawnBranch = false;
                    this.rightBranch = true;
                    this.leftBranch = false;
                }
            }
        };
        Tree.prototype.spawnSpikes = function (x, y, count) {
            for (var i = 0; i < count; i++) {
                this.spikeCollection.push(new SquirrelGame.Spikes(this.game, x + (i * 20), y));
            }
        };
        Tree.prototype.spawnAcorns = function (x, y, count) {
            for (var i = 0; i < count; i++) {
                this.acornCollection.push(new SquirrelGame.Acorn(this.game, x + (i * 30), y));
            }
        };
        Tree.prototype.update = function (squirrel) {
            var _this = this;
            //this.group.forEachAlive((member)=>{
            //    member.treeY-=100;
            //},this);
            // TODO: generating & moving & deleting trees
            this.acornCollection.forEach(function (acorn) {
                acorn.update(squirrel);
                if (acorn.sprite.width === -acorn.sprite.width || acorn.isEaten) {
                    acorn.sprite.destroy();
                    _this.acornCollection.splice(_this.acornCollection.indexOf(acorn), 1);
                }
            });
            this.spikeCollection.forEach(function (spike) {
                spike.update(squirrel);
                if (spike.sprite.width === -spike.sprite.width) {
                    spike.sprite.destroy();
                    _this.spikeCollection.splice(_this.spikeCollection.indexOf(spike), 1);
                }
            });
        };
        return Tree;
    })();
    SquirrelGame.Tree = Tree;
})(SquirrelGame || (SquirrelGame = {}));
//# sourceMappingURL=Tree.js.map