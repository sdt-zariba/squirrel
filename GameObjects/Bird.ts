/// <reference path="../Lib/phaser.d.ts"/>
///<reference path="Player.ts"/>

module SquirrelGame{
    export class Bird {
        game:Phaser.Game;
        sprite:Phaser.Sprite;
        speed = -400;

        constructor(game:Phaser.Game,y:number)
        {
            this.game = game;
            this.sprite = this.game.add.sprite(this.game.width*1.1, y, 'bird');
            this.game.physics.arcade.enable(this.sprite);
            this.sprite.body.allowGravity = false;
            this.sprite.body.checkCollision.top = false;
            this.sprite.body.checkCollision.left = false;
            this.sprite.body.checkCollision.right = false;
            this.sprite.body.checkCollision.down = false;
            this.sprite.body.velocity.x = this.speed;
            this.sprite.anchor.set(0.5,0.5);
            this.sprite.scale.set(-1,1);
            this.sprite.animations.add('Few');
        }

        update(player:Player)
        {
            this.sprite.animations.play('Few',10,true,true);
            //console.log(this.sprite.animations.frame);
            if(this.game.physics.arcade.intersects(player.squirrel.body, this.sprite.body) && !player.isDead && !player.isShieldActive)
            {
                player.isDead = true;
            }
        }
    }
}
