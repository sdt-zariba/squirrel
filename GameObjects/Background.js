/// <reference path="../Lib/phaser.d.ts"/>
var SquirrelGame;
(function (SquirrelGame) {
    var Background = (function () {
        function Background(game, background1, background2, velocity, y) {
            this.spriteY = y || 0;
            this.game = game;
            this.velocity = velocity;
            if (background1 === 'background') {
                this.image = this.game.add.sprite(0, this.spriteY, background1, 0);
                this.image.scale.set(2.5);
                this.image2 = this.game.add.sprite(this.image.width, this.spriteY, background2, 0);
                this.image2.scale.set(2.5);
                console.log(this.image.width * 2);
            }
            else {
                this.image = this.game.add.sprite(0, this.spriteY, background1, 0);
                this.image.scale.set(1.5);
                this.image.anchor.set(0, 1);
                this.image2 = this.game.add.sprite(this.image.width, this.spriteY, background2, 0);
                this.image2.scale.set(1.5);
                this.image2.anchor.set(0, 1);
                this.image3 = this.game.add.sprite(this.image.width + this.image2.width, this.spriteY, background1, 0);
                this.image3.scale.set(1.5);
                this.image3.anchor.set(0, 1);
            }
        }
        Background.prototype.moveDown = function () {
            this.image.position.y++;
            this.image2.position.y++;
            this.image3.position.y++;
        };
        Background.prototype.update = function () {
            if (this.image.key === 'background') {
                this.image.position.x -= this.velocity;
                this.image2.position.x -= this.velocity;
                if (this.image.position.x <= -this.image.width) {
                    this.image.position.x = this.image.width;
                }
                if (this.image2.position.x <= -this.image2.width) {
                    this.image2.position.x = this.image2.width;
                }
            }
            else {
                this.image.position.x -= this.velocity;
                this.image2.position.x -= this.velocity;
                this.image3.position.x -= this.velocity;
                if (this.image.position.x <= -this.image.width) {
                    this.image.position.x = this.image.width * 2;
                }
                if (this.image2.position.x <= -this.image2.width) {
                    this.image2.position.x = this.image2.width * 2;
                }
                if (this.image3.position.x <= -this.image3.width) {
                    this.image3.position.x = this.image3.width * 2;
                }
            }
        };
        return Background;
    })();
    SquirrelGame.Background = Background;
})(SquirrelGame || (SquirrelGame = {}));
//# sourceMappingURL=Background.js.map