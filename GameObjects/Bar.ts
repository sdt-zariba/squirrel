/// <reference path="../Lib/phaser.d.ts"/>
module SquirrelGame{
    export class Bar{
        game: Phaser.Game;
        backgroundFrame:Phaser.Sprite;
        backgroundColour:Phaser.Sprite;
        foregroundColour:Phaser.Sprite;

        constructor(game:Phaser.Game, x:number, y:number)
        {
            this.game = game;
            this.backgroundFrame = this.game.add.sprite(x,y,'barFrame');
            this.backgroundColour = this.game.add.sprite(x,y,'barBackgroundColour');
            this.foregroundColour = this.game.add.sprite(x,y+3,'barForegroundColour');
        }
        create()
        {
        }
    }
}
