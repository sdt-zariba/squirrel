/// <reference path="../Lib/phaser.d.ts"/>
///<reference path="Player.ts"/>
module SquirrelGame{
    export class Acorn{
        game:Phaser.Game;
        sprite:Phaser.Sprite;
        isEaten:boolean;
        coinSound:Phaser.Sound;

        constructor(game:Phaser.Game,x:number, y:number)
        {
            this.game = game;
            this.sprite = this.game.add.sprite(x,y,'acorn');
            this.game.physics.arcade.enable(this.sprite);
            this.sprite.body.allowGravity = false;
            this.sprite.body.immovable = true;
            this.sprite.body.checkCollision.top = false;
           this.sprite.anchor.set(0.5);
            this.sprite.scale.setTo(0.16);
            this.sprite.rotation=200;
            this.coinSound=this.game.add.audio('coinSound',1,false);

            this.isEaten = false;
        }

        update(player:Player) {
            //console.log(this.sprite.body.intersects(player.squirrel));

            if (this.game.physics.arcade.intersects(player.squirrel.body,this.sprite.body) && !this.isEaten)
            {
                this.coinSound.play();
                this.sprite.alpha = 0;
                this.isEaten = true;
                player.glideTime += 100;
                player.acornCount += 1;
                if(player.glideTime >= player.maxGlideTime)
                {
                    player.glideTime = player.maxGlideTime;
                }
                player.score += 300;
                player.hungerTime += 2000;
                if(player.hungerTime >= player.maxHungerTime)
                {
                    player.hungerTime = player.maxHungerTime;
                }
            }
        }
    }
}