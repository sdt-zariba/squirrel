/// <reference path="../Lib/phaser.d.ts"/>
module SquirrelGame{
    export class Emitter {
        game:Phaser.Game;
        emitter: Phaser.Particles.Arcade.Emitter;

        constructor(game:Phaser.Game){
            this.game = game;
            this.emitter = this.game.add.emitter(20,20,20);


        }

        update(){
            this.emitter.forEachAlive((particle)=>{
                particle.alpha = particle.lifespan/this.emitter.lifespan;
            },this);
        }

        start(x:number, y:number, explode:boolean, lifespan:number, quantity:number){
            this.emitter.x = x;
            this.emitter.y = y;
            this.emitter.bounce.setTo(0.9,0.9);
            this.emitter.angularDrag = 0;
            this.emitter.start(explode,lifespan,0,quantity,true);
        }
    }
}