/// <reference path="../Lib/phaser.d.ts"/>
/// <reference path = "../States/Game.ts"/>
///<reference path="Bar.ts"/>
///<reference path="Emitter.ts"/>

module SquirrelGame {
    export enum PlayerState {IDLE, RUNNING, JUMPING, FLYING}
    function RNG(from, to) {
        return Math.round(Math.random() * (to - from) + from);
    }
    export class Player {
        game:Phaser.Game;
        squirrel:Phaser.Sprite;
        currentState:PlayerState;

        jumpCounter:number;
        jumpAnimation: Phaser.Animation;
        inputBG:Phaser.Sprite;

        score:number;
        updateScoreTime:number;
        acornCount:number;

        glideTime: number;
        maxGlideTime: number;
        hungerTime: number;
        maxHungerTime: number;
        cursors: Phaser.CursorKeys;
        updateHungerBarTime:number;
        updateGlideBarTime:number;
        hungerBar: Bar;
        flightBar: Bar;
        barWidth:number;

        scale: number;

        isDead: boolean;
        hitByWind: boolean;
        leftButtonClicked:boolean;
        rightButtonClicked:boolean;
        glidingAllowed:boolean;

        isShieldTaken:boolean;
        isShieldActive:boolean;
        shieldTimer:number;
        playerShield:Phaser.Image;

        jumpSound:Phaser.Sound;
        jumpSoundPlayed:Boolean=false;

        //emitter: Phaser.Particles.Arcade.Emitter;

        constructor(game:Phaser.Game) {
            this.game = game;
            this.isDead = false;
            this.hitByWind = false;
            this.currentState = PlayerState.IDLE;
            this.scale = 0.2;

            this.score = 0;
            this.acornCount = 0;

            this.glideTime = localStorage.getItem("maxGlideTime");
            this.maxGlideTime = localStorage.getItem("maxGlideTime");
            this.hungerTime = localStorage.getItem("maxHungerTime");
            this.maxHungerTime = localStorage.getItem("maxHungerTime");
            this.updateHungerBarTime = this.game.time.now + 20;
            this.updateGlideBarTime = this.game.time.now + 20;
            this.updateScoreTime = this.game.time.now + 100;
            this.flightBar = new Bar(this.game,100,100);
            this.hungerBar = new Bar(this.game, 800,100);
            this.barWidth = this.flightBar.foregroundColour.width;

            this.cursors = this.game.input.keyboard.createCursorKeys();
            this.inputBG = this.game.add.sprite(0, 0, "");
            this.inputBG.alpha = 0;
            this.inputBG.width = 1280;
            this.inputBG.height = 720;
            this.inputBG.inputEnabled = true;
            this.inputBG.input.priorityID = 0;
            this.inputBG.events.onInputDown.add(this.jump, this);

            this.jumpCounter = 0;
            this.glidingAllowed = true;

            this.squirrel = this.game.add.sprite(0, 780, 'squirrel');
            this.squirrel.anchor.setTo(0.5, 1);
            this.squirrel.scale.setTo(this.scale);

            this.game.physics.arcade.enable(this.squirrel);
            this.squirrel.body.collideWorldBounds = true;
            this.squirrel.body.checkCollision.up = false;
            this.squirrel.body.checkCollision.left = false;
            this.squirrel.body.checkCollision.right = false;

            this.squirrel.body.setSize(350,200,-10,0);


            this.squirrel.animations.add('idle', [12], 1, false);
            this.squirrel.animations.add('running', [12, 13, 14, 15], 15, true);
            this.squirrel.animations.add('flying', [0, 1, 2, 3], 15, true);
            this.jumpAnimation = this.squirrel.animations.add('jumping', [4, 5, 6, 7, 8, 9, 10], 15, false);
            //this.game.time.events.loop(Phaser.Timer.SECOND*5, this.updateGlideTimer,this);

            this.leftButtonClicked = false;
            this.rightButtonClicked = false;

            this.isShieldTaken = false;
            this.isShieldActive = false;
            this.playerShield = this.game.add.image(this.squirrel.x,this.squirrel.y,'shield');
            this.shieldTimer = this.game.time.now - 10;
            this.playerShield.visible = false;
            this.playerShield.anchor.set(0.5, 0.8);
            this.playerShield.scale.set(0.7);

            //this.emitter = this.game.add.emitter(20,20,20);
           // this.emitter.minParticleScale=0.02;
           // this.emitter.maxParticleScale=0.09;
            //this.emitter.height=100;
            //this.emitter.width=100;

            //this.emitter.makeParticles('star');
            this.jumpSound=this.game.add.audio('jumpSound',1,false);
        }

        create() {

        }

        update() {
            this.squirrel.bringToTop();
            this.playerShield.bringToTop();
            this.updateJump();
            this.move();
            this.updateBars();
            this.updateHungerTimer();
            this.updateScore();
            this.shieldLogic();
            //this.updateEmitter();


            switch (this.currentState) {
                case PlayerState.IDLE:
                    this.squirrel.animations.play('idle');
                    break;
                case PlayerState.RUNNING:
                    this.squirrel.animations.play('running');
                    break;
                case PlayerState.JUMPING:
                    break;
                case PlayerState.FLYING:
                    this.squirrel.animations.play('flying');
                    break;
            }

        }

        //updateEmitter(){
        //    this.emitter.forEachAlive((particle)=>{
        //        particle.alpha = particle.lifespan/this.emitter.lifespan;
        //    },this);
        //}
        shieldLogic()
        {
            if(this.isShieldTaken)
            {
                this.shieldTimer = this.game.time.now + Phaser.Timer.SECOND*5;
                this.isShieldTaken = false;
            }

            if(this.shieldTimer < this.game.time.now)
            {
                this.isShieldActive = false;
            }
            else
            {
                this.isShieldActive = true;
            }

            if(this.isShieldActive)
            {
                this.playerShield.visible = true;
                this.playerShield.x = this.squirrel.x;
                this.playerShield.y = this.squirrel.y;
            }
            else
            {
                this.playerShield.visible = false;
            }
        }


        updateBars()
        {
            this.flightBar.foregroundColour.width = (this.glideTime/this.maxGlideTime)*this.barWidth;
            this.hungerBar.foregroundColour.width = (this.hungerTime/this.maxHungerTime)*this.barWidth;
        }

        updateScore()
        {
            if(this.updateScoreTime < this.game.time.now)
            {
                this.updateScoreTime = this.game.time.now +100;
                this.score += RNG(10,20);
            }
        }

        updateHungerTimer()
        {
            if(this.updateHungerBarTime < this.game.time.now)
            {
                this.updateHungerBarTime = this.game.time.now + 20;
                this.hungerTime -= 40;
            }

            if(this.hungerTime < 0)
            {
               this.hungerTime = 0;
                this.isDead = true;//Game Over Logic
            }
        }

        //updateGlideTimer()
        //{
        //    if(this.glideTime < this.maxGlideTime) {
        //        this.glideTime += 100;
        //        if(this.glideTime > this.maxGlideTime)
        //        {
        //            this.glideTime = this.maxGlideTime;
        //        }
        //    }
        //}

        updateJump() {
            if (this.squirrel.body.blocked.down || this.squirrel.body.touching.down) {
                this.jumpCounter = 0;
                this.glidingAllowed = true;
                this.jumpSoundPlayed=false;
            }

            if ( this.glidingAllowed && this.jumpCounter === 2) {
                    this.inputBG.events.onInputUp.addOnce(()=>{
                        this.glidingAllowed = false;
                    },this);

                if(this.glideTime>0) {
                    this.squirrel.body.gravity.y = 400;
                    if(!this.jumpSoundPlayed){
                        this.jumpSound.play();
                        this.jumpSoundPlayed=true;
                    }

                    if(this.updateGlideBarTime < this.game.time.now)
                    {
                        this.updateGlideBarTime = this.game.time.now + 20;
                        this.glideTime -= 40;
                    }

                    if(this.glideTime<0)
                    {
                        this.glideTime = 0;
                    }
                }
            } else {
                this.squirrel.body.gravity.y = 1000;
            }
        }

        jump() {
            if (this.jumpCounter <= 1) {
                this.currentState = PlayerState.JUMPING;
                this.squirrel.animations.play('jumping');
                this.squirrel.body.velocity.y = -500;
                this.jumpCounter += 1;
                this.jumpSound.play();
            }
        }

        move() {

            if (this.cursors.left.isDown || this.leftButtonClicked) {
                if (this.squirrel.body.blocked.down || this.squirrel.body.touching.down) {
                    this.currentState = PlayerState.RUNNING;
                }

                this.squirrel.scale.x = -this.scale;
                if(this.hitByWind) {
                    this.squirrel.body.velocity.x = -300;
                }
                else {
                    this.squirrel.body.velocity.x = -200;
                }
                this.squirrel.body.offset.x = -20;
            }
            else if (this.cursors.right.isDown || this.rightButtonClicked) {
                if (this.squirrel.body.blocked.down || this.squirrel.body.touching.down) {
                    this.currentState = PlayerState.RUNNING;
                }

                this.squirrel.scale.x = this.scale;
                if(this.hitByWind) {
                    this.squirrel.body.velocity.x = 100;
                }
                else {
                    this.squirrel.body.velocity.x = 200;
                }
                this.squirrel.body.offset.x = 20;
            }
            else if (this.squirrel.body.blocked.down || this.squirrel.body.touching.down) {
                this.currentState = PlayerState.IDLE;
                this.squirrel.body.velocity.x = 0;
            }

            if (!this.squirrel.body.blocked.down && !this.squirrel.body.touching.down && this.jumpAnimation.isFinished){
                this.currentState = PlayerState.FLYING;
            }
        }
    }
}